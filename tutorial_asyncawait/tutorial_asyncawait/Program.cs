﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace tutorial_asyncawait
{
    class Program
    {
        //static void Main(string[] args)
        //{
        //    var sw = new Stopwatch();
        //    sw.Start();
        //    Breakfast.Prepare();
        //    sw.Stop();
        //    Log($"Time elapsed {sw.ElapsedMilliseconds}");
        //    Console.ReadLine();
        //}

        static async Task Main(string[] args)
        {
            //var context = new CustomSynchronizationContext();
            //SynchronizationContext.SetSynchronizationContext(context);
            Log(SynchronizationContext.Current?.ToString());

            var b1 = new BreakfastAsync("First");
            var b2 = new BreakfastAsync("Second");


            Log("Before breakfast");
            var sw = new Stopwatch();
            sw.Start();
            await b1.PrepareAsync_1();
            //await BreakfastAsync.PrepareAsync_1();
            //var b1Task = b1.PrepareAsync_1();
            //var b2Task = b2.PrepareAsync_1();
            //await Task.WhenAll(b1Task, b2Task);
            sw.Stop();
            Log($"Time elapsed {sw.ElapsedMilliseconds}");
            Log("After breakfast");
            Log(SynchronizationContext.Current?.ToString());
            Console.ReadLine();
        }

        private static void Log(string text)
        {
            Console.WriteLine($"[{Thread.CurrentThread.ManagedThreadId}][{Thread.CurrentThread.ThreadState}] {text}");
        }
    }
}
