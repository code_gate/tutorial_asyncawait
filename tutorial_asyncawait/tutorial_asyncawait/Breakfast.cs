﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace tutorial_asyncawait
{
    static class Breakfast
    {
        const int timeMultiplier = 1000;
        public static void Prepare()
        {
            Coffee cup = PourCoffee();
            Log("coffee is ready");
            Egg eggs = FryEggs(2);
            Log("eggs are ready");
            Bacon bacon = FryBacon(3);
            Log("bacon is ready");
            Toast toast = ToastBread(2);
            ApplyButter(toast);
            ApplyJam(toast);
            Log("toast is ready");
            Juice oj = PourOJ();
            Log("oj is ready");

            Log("Breakfast is ready!");

        }

        private static Juice PourOJ()
        {
            Log("Pouring Orange Juice");
            return new Juice();
        }

        private static void ApplyJam(Toast toast)
        {
            Log("Putting jam on the toast");
        }

        private static void ApplyButter(Toast toast)
        {
            Log("Putting butter on the toast");
        }

        private static Toast ToastBread(int v)
        {
            Enumerable.Repeat("Putting a slice of bread in the toaster", v).ToList().ForEach(Log);
            Log("Start toasting...");
            Task.Delay(timeMultiplier * 2).Wait();
            Log("Remove toast from toaster");
            return new Toast();
        }

        private static Bacon FryBacon(int v)
        {
            Log($"putting {v} of bacon in the pan");
            Log("cooking first side of bacon...");
            Task.Delay(timeMultiplier * 4).Wait();
            Enumerable.Repeat("flipping a slice of bacon", v).ToList().ForEach(Log);
            Log("cooking the second side of bacon...");
            Task.Delay(timeMultiplier * 4).Wait();
            Log("Put bacon on plate");
            return new Bacon();
        }

        private static Egg FryEggs(int v)
        {
            Log("Warming the egg pan...");
            Task.Delay(timeMultiplier * 1).Wait(); //synchronous version 
            Log($"cracking {v} eggs");
            Log("cooking the eggs...");
            Task.Delay(timeMultiplier * 3).Wait();
            Log("Put eggs on plate");
            return new Egg();
        }

        private static Coffee PourCoffee()
        {
            Log("Poring coffee");
            return new Coffee();
        }

        private static void Log(string text)
        {
            Console.WriteLine($"[{Thread.CurrentThread.ManagedThreadId}][{Thread.CurrentThread.ThreadState}] {text}");
        }
    }
}
