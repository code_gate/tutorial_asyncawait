using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace tutorial_asyncawait
{
    public class BreakfastAsync
    {
        public BreakfastAsync(string name)
        {
            _name = name;
        }
        private string _name;
        const int timeMultiplier = 1000;
        public async Task PrepareAsync_1()
        {
            Coffee cup = PourCoffee();
            Log("coffee is ready");
            Egg eggs = await FryEggsAsync(2);
            Log("eggs are ready");
            Bacon bacon = await FryBaconAsync(3);
            Log("bacon is ready");
            Toast toast = await ToastBreadAsync(2);
            ApplyButter(toast);
            ApplyJam(toast);
            Log("toast is ready");
            Juice oj = PourOJ();
            Log("oj is ready");

            Log("Breakfast is ready!");
        }

        public async Task PrepareAsync_2()
        {
            Coffee cup = PourCoffee();
            Log("coffee is ready");
            Task<Egg> eggsTask = FryEggsAsync(2);
            Task<Bacon> baconTask = FryBaconAsync(3);
            Task<Toast> toastTask = ToastBreadAsync(2);
            toastTask = toastWithButterAndJam(toastTask);
            Juice oj = PourOJ();
            Log("oj is ready");
            await Task.WhenAll(eggsTask, toastTask, baconTask);
            Log("Breakfast is ready!");
        }

        async Task<Toast> toastWithButterAndJam(Task<Toast> breadToasting)
        {
            var toast = await breadToasting;
            ApplyButter(toast);
            ApplyJam(toast);
            return toast;
        }

        private Juice PourOJ()
        {
            Log("Pouring Orange Juice");
            return new Juice();
        }

        private void ApplyJam(Toast toast)
        {
            Log("Putting jam on the toast");
        }

        private void ApplyButter(Toast toast)
        {
            Log("Putting butter on the toast");
        }

        private async Task<Toast> ToastBreadAsync(int v)
        {
            Enumerable.Repeat("Putting a slice of bread in the toaster", v).ToList().ForEach(Log);
            Log("Start toasting...");
            await Task.Delay(timeMultiplier * 2);
            //await Task.Run(() => Task.Delay(timeMultiplier * 2));
            Log("Remove toast from toaster");
            Log("toast is ready");
            return new Toast();
        }

        private async Task<Bacon> FryBaconAsync(int v)
        {
            Log($"putting {v} of bacon in the pan");
            Log("cooking first side of bacon...");
            await Task.Delay(timeMultiplier * 4);
            //await Task.Run(() => Task.Delay(timeMultiplier * 4));
            Enumerable.Repeat("flipping a slice of bacon", v).ToList().ForEach(Log);
            Log("cooking the second side of bacon...");
            await Task.Delay(timeMultiplier * 4);
            //await Task.Run(() => Task.Delay(timeMultiplier * 4));
            Log("Put bacon on plate");
            Log("bacon is ready");
            return new Bacon();
        }

        private async Task<Egg> FryEggsAsync(int v)
        {
            Log("Warming the egg pan...");
            await Task.Delay(timeMultiplier * 1).ConfigureAwait(false);
            //await Task.Run(() => Task.Delay(timeMultiplier * 1));
            Log($"cracking {v} eggs");
            Log("cooking the eggs...");
            await Task.Delay(timeMultiplier * 3).ConfigureAwait(false);
            //await Task.Run(() => Task.Delay(timeMultiplier * 3));
            Log("Put eggs on plate");
            Log("eggs are ready");
            return new Egg();
        }

        private Coffee PourCoffee()
        {
            Log("Poring coffee");
            return new Coffee();
        }

        private void Log(string text)
        {
            Console.WriteLine($"[{Thread.CurrentThread.ManagedThreadId}][{Thread.CurrentThread.ThreadState}] [{_name}] {text}");
        }
    }
}
